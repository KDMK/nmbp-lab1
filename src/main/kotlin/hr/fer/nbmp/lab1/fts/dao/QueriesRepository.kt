package hr.fer.nbmp.lab1.fts.dao

import hr.fer.nbmp.lab1.fts.dao.dbo.QueryReportByDay
import hr.fer.nbmp.lab1.fts.dao.dbo.QueryReportByHours
import hr.fer.nbmp.lab1.fts.dao.impl.QueriesRepositoryImpl
import hr.fer.nbmp.lab1.fts.domain.Query

interface QueriesRepository {
    fun getQuery(id: Long): Query?
    fun getAllQueries(): MutableList<Query>
    fun crossTab(startDateString: String, endDateString: String): MutableList<QueryReportByDay>
    fun crossTabHours(date: String): MutableList<QueryReportByHours>
}
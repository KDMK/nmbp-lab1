package hr.fer.nbmp.lab1.fts.dao.mapper

import hr.fer.nbmp.lab1.fts.domain.Query
import org.springframework.jdbc.core.RowMapper
import java.sql.ResultSet
import java.time.LocalDateTime
import java.util.*

class QueryMapper : RowMapper<Query> {

    override fun mapRow(rs: ResultSet, rowNum: Int): Query? = Query(
            rs.getLong("id"),
            rs.getString("source"),
            rs.getString("query"),
            LocalDateTime.ofInstant(rs.getTimestamp("created_at").toInstant(), TimeZone.getDefault().toZoneId())
    )
}
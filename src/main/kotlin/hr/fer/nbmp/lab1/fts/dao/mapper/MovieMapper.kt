package hr.fer.nbmp.lab1.fts.dao.mapper

import hr.fer.nbmp.lab1.fts.domain.Movie
import org.springframework.jdbc.core.RowMapper
import java.sql.SQLException
import java.sql.ResultSet

class MovieMapper : RowMapper<Movie> {
    @Throws(SQLException::class)
    override fun mapRow(rs: ResultSet, rowNum: Int): Movie {
        val movie = Movie()

        movie.movieid = rs.getInt("movieid")
        movie.title = rs.getString("title")
        movie.categories = rs.getString("categories")
        movie.description = rs.getString("description")
        movie.summary = rs.getString("summary")
        try {
            movie.rank = rs.getDouble("rank")
        } catch (ignorable: Exception) {
        }

        return movie
    }
}
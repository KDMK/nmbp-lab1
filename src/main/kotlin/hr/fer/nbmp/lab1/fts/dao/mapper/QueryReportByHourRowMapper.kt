package hr.fer.nbmp.lab1.fts.dao.mapper

import hr.fer.nbmp.lab1.fts.dao.dbo.NumOfQueriesInHour
import hr.fer.nbmp.lab1.fts.dao.dbo.QueryReportByHours
import hr.fer.nbmp.lab1.fts.dao.impl.QueriesRepositoryImpl
import org.springframework.jdbc.core.RowMapper
import java.sql.ResultSet
import java.util.*

class QueryReportByHourRowMapper constructor(private val timeLabels: TreeSet<String>) : RowMapper<QueryReportByHours> {

    override fun mapRow(rs: ResultSet, rowNum: Int): QueryReportByHours? {
        val queryReport = QueryReportByHours()

        queryReport.query = rs.getString(QueriesRepositoryImpl.QUERY_COLUMN_NAME)
        for (label in timeLabels) {
            val dateVal = NumOfQueriesInHour(label, rs.getInt(label))
            queryReport.numOfQueriesByDay!!.add(dateVal)
        }

        return queryReport
    }
}
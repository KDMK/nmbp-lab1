package hr.fer.nbmp.lab1.fts.controller

import hr.fer.nbmp.lab1.fts.dao.dbo.QueryReportByDay
import hr.fer.nbmp.lab1.fts.dao.dbo.QueryReportByHours
import hr.fer.nbmp.lab1.fts.service.QueryReportService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/report")
class QueryReportController @Autowired constructor(private val queryReportService: QueryReportService) {

    data class QueryReportForRangeRequestDTO(var startDate: String? = null, var endDate: String? = null)
    data class QueryReportForDayRequestDTO(var requestDate: String? = null)

    @PostMapping("/range")
    fun getCrossTabForRange(@RequestBody queryReportForRangeRequestDTO: QueryReportForRangeRequestDTO): ResponseEntity<MutableList<QueryReportByDay>> {
        return ResponseEntity.ok(queryReportService.getReport(queryReportForRangeRequestDTO.startDate!!, queryReportForRangeRequestDTO.endDate!!))
    }

    @PostMapping("/single")
    fun getCrossTabForDay(@RequestBody queryReportForDayRequestDTO: QueryReportForDayRequestDTO): ResponseEntity<MutableList<QueryReportByHours>> {
        return ResponseEntity.ok(queryReportService.getReportForDay(queryReportForDayRequestDTO.requestDate!!))
    }
}
package hr.fer.nbmp.lab1.fts.controller

import hr.fer.nbmp.lab1.fts.domain.Query
import hr.fer.nbmp.lab1.fts.service.QueryService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/query")
class QueryController constructor(private val queryService: QueryService) {

    @GetMapping
    fun getAllQueries(): ResponseEntity<MutableList<Query>> {
        return ResponseEntity.ok(queryService.getAllQueries())
    }

    @GetMapping("/{id}")
    fun getSingleQuery(@PathVariable id: Long): ResponseEntity<Query> {
        return ResponseEntity.ok(queryService.getSingleQuery(id).orElseGet(null))
    }
}
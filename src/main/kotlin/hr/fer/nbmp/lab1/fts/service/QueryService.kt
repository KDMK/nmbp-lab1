package hr.fer.nbmp.lab1.fts.service

import hr.fer.nbmp.lab1.fts.domain.Query
import java.util.*

interface QueryService {

    fun getAllQueries(): MutableList<Query>
    fun getSingleQuery(id: Long): Optional<Query>
}
package hr.fer.nbmp.lab1.fts.service

import hr.fer.nbmp.lab1.fts.dao.dbo.QueryReportByDay
import hr.fer.nbmp.lab1.fts.dao.dbo.QueryReportByHours

interface QueryReportService {
    fun getReport(startDate: String, endDate: String): MutableList<QueryReportByDay>
    fun getReportForDay(date: String): MutableList<QueryReportByHours>
}
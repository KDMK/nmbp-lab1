package hr.fer.nbmp.lab1.fts.domain

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Table

@Table(name = "queries", schema = "nmbp")
data class Query(
        @Column var id: Long = 0,
        @Column var source: String,
        @Column var query: String,
        @Column @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy hh:mm:ss") var timestamp: LocalDateTime
)
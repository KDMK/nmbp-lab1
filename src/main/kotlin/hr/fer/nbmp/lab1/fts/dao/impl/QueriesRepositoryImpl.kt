package hr.fer.nbmp.lab1.fts.dao.impl

import hr.fer.nbmp.lab1.fts.dao.QueriesRepository
import hr.fer.nbmp.lab1.fts.dao.dbo.NumOfQueriesInDay.Companion.OUTPUT_DATE_FORMAT
import hr.fer.nbmp.lab1.fts.dao.dbo.QueryReportByDay
import hr.fer.nbmp.lab1.fts.dao.dbo.QueryReportByHours
import hr.fer.nbmp.lab1.fts.dao.mapper.QueryMapper
import hr.fer.nbmp.lab1.fts.dao.mapper.QueryReportByDayRowMapper
import hr.fer.nbmp.lab1.fts.dao.mapper.QueryReportByHourRowMapper
import hr.fer.nbmp.lab1.fts.domain.Query
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import java.time.LocalDate
import java.util.*

@Repository
class QueriesRepositoryImpl @Autowired constructor(private val jdbcTemplate: JdbcTemplate) : QueriesRepository {

    companion object {
        private const val REPLACEMENT_STRING = "%%COLUMN_LIST%%"
        const val QUERY_COLUMN_NAME = "a_query"
        private const val CROSSTAB_TIME_COLUMN_FORMAT = "t%02d:00"

        private val INPUT_DATE_FORMAT = java.time.format.DateTimeFormatter.ofPattern("dd.MM.yyyy")

        private const val GET_ALL_QUERIES = "SELECT * FROM nmbp.queries;"
        private const val GET_SINGLE_QUERY = "SELECT * FROM nmbp.queries WHERE id = ?;"

        private const val CROSSTAB_QUERY_TEMPLATE = "\nSELECT * FROM crosstab(${'$'}${'$'}\n" +
                "   SELECT query,\n" +
                "          date_trunc('day', created_at) date,\n" +
                "          count(query) num\n" +
                "   FROM nmbp.queries\n" +
                "   WHERE created_at > to_date(\$\$ || '''' || ? || '''' || \$\$, 'DD.MM.YYYY')\n" +
                "   AND   created_at < to_date(\$\$ || '''' || ? || '''' || \$\$, 'DD.MM.YYYY')\n" +
                "   GROUP BY query, date\n" +
                "   ORDER BY 1;\n" +
                "${'$'}${'$'},${'$'}${'$'}\n" +
                "   SELECT GENERATE_SERIES (\n" +
                "     to_timestamp(\$\$ || '''' || ? || '''' || \$\$, 'DD.MM.YYYY'),  -- initial day\n" +
                "     to_timestamp(\$\$ || '''' || ? || '''' || \$\$, 'DD.MM.YYYY'),  -- final day\n" +
                "     '1 day'\n" +
                "   )::Timestamp without time zone\n" +
                "${'$'}${'$'}\n" +
                ") AS ct(\n" +
                " a_query TEXT\n" +
                " $REPLACEMENT_STRING" + ");\n"

        private const val CROSSTAB_BY_HOURS_QUERY_TEMPLATE = "SELECT * FROM CROSSTAB(\$\$\n" +
                "  SELECT query, extract(hour from created_at) as hour, count(query) num FROM nmbp.queries\n" +
                "    WHERE created_at > to_date(\$\$ || '''' || ? || '''' || \$\$, 'DD.MM.YYYY')\n" +
                "    AND created_at < to_date(\$\$ || '''' || ? || '''' || \$\$, 'DD.MM.YYYY')\n" +
                "  GROUP BY query, hour\n" +
                "  ORDER BY 2,1;\n" +
                "  \$\$,\$\$\n" +
                "    SELECT GENERATE_SERIES (0,23,1)\n" +
                "  \$\$\n" +
                ") AS bla(\n" +
                "  a_query TEXT\n" +
                ", \"t00:00\" BIGINT\n" +
                ", \"t01:00\" NUMERIC\n" +
                ", \"t02:00\" NUMERIC\n" +
                ", \"t03:00\" NUMERIC\n" +
                ", \"t04:00\" NUMERIC\n" +
                ", \"t05:00\" NUMERIC\n" +
                ", \"t06:00\" NUMERIC\n" +
                ", \"t07:00\" NUMERIC\n" +
                ", \"t08:00\" NUMERIC\n" +
                ", \"t09:00\" NUMERIC\n" +
                ", \"t10:00\" NUMERIC\n" +
                ", \"t11:00\" NUMERIC\n" +
                ", \"t12:00\" NUMERIC\n" +
                ", \"t13:00\" NUMERIC\n" +
                ", \"t14:00\" NUMERIC\n" +
                ", \"t15:00\" NUMERIC\n" +
                ", \"t16:00\" NUMERIC\n" +
                ", \"t17:00\" NUMERIC\n" +
                ", \"t18:00\" NUMERIC\n" +
                ", \"t19:00\" NUMERIC\n" +
                ", \"t20:00\" NUMERIC\n" +
                ", \"t21:00\" NUMERIC\n" +
                ", \"t22:00\" NUMERIC\n" +
                ", \"t23:00\" NUMERIC\n" +
                ");"
    }

    override fun getQuery(id: Long): Query? {
        return jdbcTemplate.queryForObject(GET_SINGLE_QUERY, arrayOf(id), QueryMapper())
    }

    override fun getAllQueries(): MutableList<Query> {
        return jdbcTemplate.query(GET_ALL_QUERIES, QueryMapper())
    }

    override fun crossTab(startDateString: String, endDateString: String): MutableList<QueryReportByDay> {
        val endDate = LocalDate.parse(endDateString, INPUT_DATE_FORMAT)
        val dateLabels: TreeSet<String> = generateDateLabels(startDateString, endDate)

        val replacementString = dateLabels.fold("") { acc: String, it ->
            "$acc, $it NUMERIC\n"
        }

        val crosstabQuery = CROSSTAB_QUERY_TEMPLATE.replace(REPLACEMENT_STRING, replacementString)
        return jdbcTemplate.query(crosstabQuery,
                arrayOf(startDateString, endDateString, startDateString, endDateString),
                QueryReportByDayRowMapper(dateLabels))
    }

    override fun crossTabHours(date: String): MutableList<QueryReportByHours> {
        val timeLabels: TreeSet<String> = TreeSet()

        for (i in 0..23) {
            timeLabels.add(String.format(CROSSTAB_TIME_COLUMN_FORMAT, i))
        }

        val endDate = LocalDate.from(INPUT_DATE_FORMAT.parse(date))

        return jdbcTemplate.query(CROSSTAB_BY_HOURS_QUERY_TEMPLATE,
                arrayOf(date, INPUT_DATE_FORMAT.format(endDate.plusDays(1L))),
                QueryReportByHourRowMapper(timeLabels))
    }

    private fun generateDateLabels(startDateString: String, endDate: LocalDate): TreeSet<String> {
        val dateLabels: TreeSet<String> = TreeSet()
        var tempDate = LocalDate.parse(startDateString, INPUT_DATE_FORMAT)
        while (tempDate.isBefore(endDate.plusDays(1L))) {
            val dateString = "d" + tempDate.format(OUTPUT_DATE_FORMAT)
            dateLabels.add(dateString)
            tempDate = tempDate.plusDays(1L)
        }
        return dateLabels
    }

}
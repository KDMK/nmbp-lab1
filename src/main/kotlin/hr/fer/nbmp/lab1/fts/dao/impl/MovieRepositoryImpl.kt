package hr.fer.nbmp.lab1.fts.dao.impl

import hr.fer.nbmp.lab1.fts.controller.dto.QueryEntity
import hr.fer.nbmp.lab1.fts.dao.MovieRepository
import hr.fer.nbmp.lab1.fts.dao.mapper.MovieMapper
import hr.fer.nbmp.lab1.fts.domain.Movie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import java.time.Instant
import java.util.*

@Repository
class MovieRepositoryImpl @Autowired constructor(private var jdbcTemplate: JdbcTemplate) : MovieRepository {

    companion object {
        private const val SCHEMA_NAME = "nmbp"
        private const val MOVIE_TABLE_NAME = "movie"
        private const val QUERY_TABLE_NAME = "queries"

        private const val REPLACEMENT_PATTERN = "%%ADDITIONAL_PATTERN%%"

        private const val QUERY_ALL_MOVIES = "SELECT * FROM $SCHEMA_NAME.$MOVIE_TABLE_NAME"
        private const val QUERY_BY_ID = "SELECT * FROM $SCHEMA_NAME.$MOVIE_TABLE_NAME WHERE movieid = ?"
        private const val INSERT_NEW_MOVIE = "INSERT INTO $SCHEMA_NAME.$MOVIE_TABLE_NAME " +
                "(title, categories, summary, description) VALUES (?, ?, ?, ?);"
        private const val INSERT_USER_QUERY = "INSERT INTO $SCHEMA_NAME.$QUERY_TABLE_NAME(query, source, created_at) VALUES(?, ?, to_timestamp(?))"
        private const val FUZZY_SEARCH = "SELECT movieid, title, summary, description, categories FROM nmbp.movie WHERE ? " +
                "% ANY(string_to_array(summary, ' ')) LIMIT 5;"
        private const val SEARCH_TEMPLATE = "SELECT movieid,\n" +
                "       ts_headline(title, to_tsquery('english', ?)) title,\n" +
                "       ts_headline(summary, to_tsquery('english', ?)) summary,\n" +
                "       description,\n" +
                "       categories,\n" +
                "       ts_rank(searchvector, to_tsquery('english', ?)) rank\n" +
                "FROM nmbp.movie\n" +
                "WHERE searchvector @@ to_tsquery('english', ?)\n" +
                REPLACEMENT_PATTERN +
                "ORDER BY rank DESC;"
    }

    override fun findAll(): List<Movie> {
        return jdbcTemplate.query(QUERY_ALL_MOVIES, MovieMapper())
    }

    override fun findById(id: Int): Optional<Movie> {
        return Optional.ofNullable(jdbcTemplate.queryForObject(QUERY_BY_ID, arrayOf(id), MovieMapper()))
    }

    override fun createNewMovie(movie: Movie) {
        jdbcTemplate.update(INSERT_NEW_MOVIE, movie.title, movie.categories, movie.summary, movie.description)
    }

    override fun fuzzySearch(term: String): List<Movie> {
        return jdbcTemplate.query(FUZZY_SEARCH, arrayOf(term), MovieMapper())
    }

    override fun searchMovie(queryEntity: QueryEntity, sourceIp: String): ResponseDTO {
        var additionalQueries = ""

        if (queryEntity.queryElements.size > 1) {
            for (i in 1 until queryEntity.queryElements.size) {
                additionalQueries += "${queryEntity.operator} searchvector @@ to_tsquery('english', ?)\n"
            }
        }

        val searchQuery = SEARCH_TEMPLATE.replace(REPLACEMENT_PATTERN, additionalQueries)

        var tempQuery = "" + searchQuery // hacky
        val queryParams = arrayOf(queryEntity.query, queryEntity.query, queryEntity.query, *queryEntity.queryElements.toTypedArray())
        var i = 0
        while (tempQuery.contains("?")) {
            tempQuery = tempQuery.replaceFirst("?", queryParams[i++])
        }

        jdbcTemplate.update(INSERT_USER_QUERY, queryEntity.query, sourceIp, Instant.now().epochSecond)
        return ResponseDTO(tempQuery, jdbcTemplate.query(searchQuery, queryParams, MovieMapper()))
    }


    data class ResponseDTO(var query: String, var searchResult: List<Movie>)
}

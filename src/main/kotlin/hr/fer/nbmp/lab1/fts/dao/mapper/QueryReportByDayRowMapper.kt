package hr.fer.nbmp.lab1.fts.dao.mapper

import hr.fer.nbmp.lab1.fts.dao.dbo.NumOfQueriesInDay
import hr.fer.nbmp.lab1.fts.dao.dbo.QueryReportByDay
import hr.fer.nbmp.lab1.fts.dao.impl.QueriesRepositoryImpl
import org.springframework.jdbc.core.RowMapper
import java.sql.ResultSet
import java.util.*

class QueryReportByDayRowMapper(private val dateLabels: TreeSet<String>) : RowMapper<QueryReportByDay> {

    override fun mapRow(rs: ResultSet, rowNum: Int): QueryReportByDay? {
        val queryReport = QueryReportByDay()

        queryReport.query = rs.getString(QueriesRepositoryImpl.QUERY_COLUMN_NAME)
        for (label in dateLabels) {
            val dateVal = NumOfQueriesInDay(label, rs.getInt(label))
            queryReport.numOfQueriesByDay!!.add(dateVal)
        }
        return queryReport
    }
}
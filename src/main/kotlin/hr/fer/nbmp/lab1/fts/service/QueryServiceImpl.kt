package hr.fer.nbmp.lab1.fts.service

import hr.fer.nbmp.lab1.fts.dao.QueriesRepository
import hr.fer.nbmp.lab1.fts.domain.Query
import org.springframework.stereotype.Service
import java.util.*

@Service
class QueryServiceImpl constructor(private val queriesRepository: QueriesRepository) : QueryService {

    override fun getAllQueries(): MutableList<Query> {
        return queriesRepository.getAllQueries()
    }

    override fun getSingleQuery(id: Long): Optional<Query> {
        return Optional.ofNullable(queriesRepository.getQuery(id))
    }
}
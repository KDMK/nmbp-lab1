package hr.fer.nbmp.lab1.fts.service

import hr.fer.nbmp.lab1.fts.controller.dto.QueryEntity
import hr.fer.nbmp.lab1.fts.dao.impl.MovieRepositoryImpl
import hr.fer.nbmp.lab1.fts.domain.Movie
import org.springframework.stereotype.Service

@Service
interface MovieService {

    fun getAllMovies(): List<Movie>
    fun getMovieById(id: Int): Movie?
    fun createMovie(movie: Movie)
    fun fuzzySearch(term: String): List<Movie>
    fun searchMovie(queryEntity: QueryEntity, sourceIp: String): MovieRepositoryImpl.ResponseDTO
}
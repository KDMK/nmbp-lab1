package hr.fer.nbmp.lab1.fts.controller

import hr.fer.nbmp.lab1.fts.controller.dto.QueryEntity
import hr.fer.nbmp.lab1.fts.dao.impl.MovieRepositoryImpl
import hr.fer.nbmp.lab1.fts.domain.Movie
import hr.fer.nbmp.lab1.fts.service.MovieService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpRequest
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@RestController
@RequestMapping("/api/movie")
class MovieController @Autowired constructor(var movieService: MovieService) {

    companion object {
        private val log = LoggerFactory.getLogger(MovieController::class.java)
    }

    @GetMapping
    fun getAllMovies(): ResponseEntity<List<Movie>> = ResponseEntity.ok(movieService.getAllMovies())

    @GetMapping("/{id}")
    fun getMovieById(@PathVariable id: Int) = movieService.getMovieById(id)?.let { ResponseEntity.ok(it) }

    @PostMapping
    fun createNewMovie(@RequestBody @Valid movie:Movie) {
        movieService.createMovie(movie)
    }

    @GetMapping("/fzf/{term}")
    fun fuzzySearchMovie(@PathVariable term: String): List<Movie> {
        return movieService.fuzzySearch(term)
    }

    @PostMapping("/search")
    fun fullSearchMovie(@RequestBody queryEntity: QueryEntity, httpRequest: HttpServletRequest): MovieRepositoryImpl.ResponseDTO {
        return movieService.searchMovie(queryEntity, httpRequest.remoteAddr)
    }
}
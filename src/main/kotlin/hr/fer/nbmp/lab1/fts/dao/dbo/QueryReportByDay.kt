package hr.fer.nbmp.lab1.fts.dao.dbo

import java.time.LocalDate
import java.util.*

class QueryReportByDay {
    var query: String = ""
    var numOfQueriesByDay: TreeSet<NumOfQueriesInDay>? = TreeSet()
}

class NumOfQueriesInDay(var date: String?, var numOfQueries: Int?) : Comparable<NumOfQueriesInDay> {
    companion object {
        val OUTPUT_DATE_FORMAT = java.time.format.DateTimeFormatter.ofPattern("dd_MM_yyyy")!!
    }

    override fun compareTo(other: NumOfQueriesInDay): Int {
        val thisDate = LocalDate.from(OUTPUT_DATE_FORMAT.parse(this.date?.substring(1)))
        val otherDate = LocalDate.from(OUTPUT_DATE_FORMAT.parse(other.date?.substring(1)))

        return thisDate.compareTo(otherDate)
    }
}


package hr.fer.nbmp.lab1.fts.dao

import hr.fer.nbmp.lab1.fts.controller.dto.QueryEntity
import hr.fer.nbmp.lab1.fts.dao.impl.MovieRepositoryImpl
import hr.fer.nbmp.lab1.fts.domain.Movie
import java.util.*

interface MovieRepository {

    fun findAll(): List<Movie>
    fun findById(id: Int): Optional<Movie>
    fun createNewMovie(movie: Movie)
    fun fuzzySearch(term: String): List<Movie>
    fun searchMovie(queryEntity: QueryEntity, sourceIp: String): MovieRepositoryImpl.ResponseDTO
}
package hr.fer.nbmp.lab1.fts

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.stereotype.Controller
import org.springframework.web.servlet.config.annotation.EnableWebMvc

@SpringBootApplication
@EnableWebMvc
@Controller
class FtsApplication

fun main(args: Array<String>) {
    runApplication<FtsApplication>(*args)
}

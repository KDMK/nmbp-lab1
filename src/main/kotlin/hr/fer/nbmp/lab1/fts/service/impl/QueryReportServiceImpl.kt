package hr.fer.nbmp.lab1.fts.service.impl

import hr.fer.nbmp.lab1.fts.dao.QueriesRepository
import hr.fer.nbmp.lab1.fts.dao.dbo.QueryReportByDay
import hr.fer.nbmp.lab1.fts.dao.dbo.QueryReportByHours
import hr.fer.nbmp.lab1.fts.service.QueryReportService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class QueryReportServiceImpl @Autowired constructor(private val queriesRepository: QueriesRepository) : QueryReportService {

    override fun getReport(startDate: String, endDate: String): MutableList<QueryReportByDay> {
        return queriesRepository.crossTab(startDate, endDate)
    }

    override fun getReportForDay(date: String): MutableList<QueryReportByHours> {
        return queriesRepository.crossTabHours(date)
    }
}
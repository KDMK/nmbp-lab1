package hr.fer.nbmp.lab1.fts.service.impl

import hr.fer.nbmp.lab1.fts.controller.dto.QueryEntity
import hr.fer.nbmp.lab1.fts.dao.MovieRepository
import hr.fer.nbmp.lab1.fts.dao.impl.MovieRepositoryImpl
import hr.fer.nbmp.lab1.fts.domain.Movie
import hr.fer.nbmp.lab1.fts.service.MovieService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MovieServiceImpl @Autowired constructor(private val movieRepository: MovieRepository) : MovieService {

    override fun getAllMovies(): List<Movie> {
        return movieRepository.findAll()
    }

    override fun getMovieById(id: Int): Movie? {
        return movieRepository.findById(id).orElse(null)
    }

    override fun createMovie(movie: Movie) {
        movieRepository.createNewMovie(movie)
    }

    override fun fuzzySearch(term: String): List<Movie> {
        return movieRepository.fuzzySearch(term)
    }

    override fun searchMovie(queryEntity: QueryEntity, sourceIp: String): MovieRepositoryImpl.ResponseDTO {
        return movieRepository.searchMovie(queryEntity, sourceIp)
    }
}
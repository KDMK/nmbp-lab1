package hr.fer.nbmp.lab1.fts.dao.dbo

import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*

class QueryReportByHours {
    var query: String = ""
    var numOfQueriesByDay: TreeSet<NumOfQueriesInHour>? = TreeSet()
}

class NumOfQueriesInHour(var time: String?, var numOfQueries: Int?) : Comparable<NumOfQueriesInHour> {
    override fun compareTo(other: NumOfQueriesInHour): Int {
        val thisDate = LocalTime.parse(this.time?.substring(1), DateTimeFormatter.ofPattern("HH:mm"))
        val otherDate = LocalTime.parse(other.time?.substring(1), DateTimeFormatter.ofPattern("HH:mm"))

        return thisDate.compareTo(otherDate)
    }
}

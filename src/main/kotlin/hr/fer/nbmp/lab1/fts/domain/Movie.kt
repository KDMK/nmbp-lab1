package hr.fer.nbmp.lab1.fts.domain

import org.hibernate.validator.constraints.Length
import javax.persistence.Column
import javax.persistence.Table

@Table(name = "movie", schema = "nmbp")
data class Movie(
        @Column var movieid: Int = 0,
        @Column @Length(max = 255) var title: String? = null,
        @Column @Length(max = 255) var categories: String? = null,
        @Column @Length(max = 255) var summary: String? = null,
        @Column @Length(max = 255) var description: String? = null,
        var rank: Double? = null
)
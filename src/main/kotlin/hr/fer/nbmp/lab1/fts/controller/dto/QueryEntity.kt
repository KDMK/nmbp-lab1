package hr.fer.nbmp.lab1.fts.controller.dto

data class QueryEntity(
        val query: String = "",
        val queryElements: List<String> = emptyList(),
        val operator: String = "OR"
)
CREATE OR REPLACE FUNCTION update_searchvector()
  RETURNS trigger AS
$$
BEGIN
  UPDATE nmbp.movie SET searchvector = to_tsvector(title || ' ' || categories || ' ' || summary || ' ' || description) WHERE movieid = NEW.movieid;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER u_searchvector
AFTER INSERT
    ON nmbp.movie
    FOR EACH ROW
EXECUTE PROCEDURE update_searchvector();

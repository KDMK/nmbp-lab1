import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Movie} from "../../domain/movie";
import {MovieServiceImpl} from "../../service/movie-service-impl.service";
import {ActivatedRoute} from "@angular/router";
import {Observable, of, Subject} from "rxjs";
import {QueryParserService} from "../../service/query-parser.service";
import {SearchDTO} from "../../domain/searchDTO";

@Component({
  selector: 'app-movie-list-component',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {

  public movies: Observable<Array<Movie>>;
  public query: Observable<String>;

  constructor(private movieService: MovieServiceImpl, private route: ActivatedRoute, private queryParser: QueryParserService, private location: Location) {
    route.paramMap.subscribe((data) => {
      let parsedValue = this.queryParser.parse(data.get("searchQuery"));
      this.movieService.movieSearch(this.queryParser.makeExpression(parsedValue, data.get("operationSelector") == "true" ? "OR" : "AND")).toPromise().then((response) => {
        this.movies = of(response.searchResult);
        this.query = of(response.query);
      });
    });
  }

  ngOnInit() {
  }

  navigateBack() {
    this.location.back();
  }
}

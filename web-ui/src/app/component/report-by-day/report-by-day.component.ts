import {Component, OnInit} from '@angular/core';
import {MovieReportStructureByDay} from "../../domain/movieReportStructureByDay";
import {MovieReportServiceImplService} from "../../service/movie-report-service-impl.service";

@Component({
  selector: 'app-report-by-day',
  templateUrl: './report-by-day.component.html',
  styleUrls: ['./report-by-day.component.css']
})
export class ReportByDayComponent implements OnInit {

  public columns: Array<string>;
  public data: Array<MovieReportStructureByDay>;

  public dateStartString: string;
  public dateEndString: string;

  constructor(private movieReportService: MovieReportServiceImplService) { }

  ngOnInit() {

  }

  onChange() {
    let startDate = new Date((Date.parse(this.dateStartString)));
    let endDate = new Date(Date.parse(this.dateEndString));

    let startDateString = `${startDate.getDate() < 10 ? '0' : ''}${startDate.getDate()}.${startDate.getMonth() < 9 ? '0' : ''}${startDate.getMonth() + 1}.${startDate.getFullYear()}`;
    let endDateString = `${endDate.getDate() < 10 ? '0' : ''}${endDate.getDate()}.${endDate.getMonth() < 9 ? '0' : ''}${endDate.getMonth() + 1}.${endDate.getFullYear()}`;

    console.log(startDateString);
    console.log(endDateString);

    this.movieReportService.getReportForRange(startDateString, endDateString).toPromise().then(response => {
      this.columns = response[0].numOfQueriesByDay.map((e) => e.date);
      this.data = response;
    })

  }
}

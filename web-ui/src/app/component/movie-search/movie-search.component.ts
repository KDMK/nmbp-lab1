import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {MovieServiceImpl} from "../../service/movie-service-impl.service";

import {debounceTime, distinctUntilChanged} from "rxjs/operators";
import {QueryParserService} from "../../service/query-parser.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.css']
})
export class MovieSearchComponent implements OnInit {

  results: Array<any>;
  queryField: FormControl = new FormControl();
  operationSelector: FormControl = new FormControl();

  public searchForm: FormGroup;

  constructor(private movieService: MovieServiceImpl, private queryParser: QueryParserService, private router: Router) {
    this.operationSelector.setValue(false);
    this.searchForm = new FormGroup({
      queryField: this.queryField,
      operationSelector: this.operationSelector
    });
  }

  ngOnInit() {
    this.queryField.valueChanges
      .pipe(
        debounceTime(200),
        distinctUntilChanged())
      .subscribe(queryField => {
        if (queryField.length < 1) return;

        this.movieService.fuzzySearch(queryField).subscribe((result) => {
          this.results = result;
        })
      });
  }

  submitSearch(event) {
    if (event.keyCode == 13) {
      this.router.navigate(['/movie/list', {searchQuery: this.queryField.value, operationSelector: this.operationSelector.value.toString()}])
    }
  }

}

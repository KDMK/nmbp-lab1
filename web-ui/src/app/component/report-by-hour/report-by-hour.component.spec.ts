import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportByHourComponent } from './report-by-hour.component';

describe('ReportByHourComponent', () => {
  let component: ReportByHourComponent;
  let fixture: ComponentFixture<ReportByHourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportByHourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportByHourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

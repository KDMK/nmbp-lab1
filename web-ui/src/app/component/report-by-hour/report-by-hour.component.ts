import { Component, OnInit } from '@angular/core';
import {MovieReportStructureByDay} from "../../domain/movieReportStructureByDay";
import {MovieReportServiceImplService} from "../../service/movie-report-service-impl.service";

@Component({
  selector: 'app-report-by-hour',
  templateUrl: './report-by-hour.component.html',
  styleUrls: ['./report-by-hour.component.css']
})
export class ReportByHourComponent implements OnInit {

  public columns: Array<string>;
  public data: Array<MovieReportStructureByDay>;

  public queryDateString: string;

  constructor(private movieReportService: MovieReportServiceImplService) { }

  ngOnInit() {

  }

  onChange() {
    console.log(this.queryDateString);
    let startDate = new Date((Date.parse(this.queryDateString)));

    let startDateString = `${startDate.getDate() < 10 ? '0' : ''}${startDate.getDate()}.${startDate.getMonth() < 9 ? '0' : ''}${startDate.getMonth() + 1}.${startDate.getFullYear()}`;

    console.log(startDateString);

    this.movieReportService.getReportForSingle(startDateString).toPromise().then(response => {
      this.columns = response[0].numOfQueriesByDay.map((e) => e.time);
      this.data = response;
    })

  }

}

import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {MovieServiceImpl} from "../../service/movie-service-impl.service";
import {Movie} from "../../domain/movie";

@Component({
  selector: 'app-movie-create',
  templateUrl: './movie-create.component.html',
  styleUrls: ['./movie-create.component.css']
})
export class MovieCreateComponent implements OnInit {

  public movieForm: FormGroup;

  constructor(private movieService: MovieServiceImpl) {
  }

  ngOnInit() {
    this.movieForm = new FormGroup({
        title: new FormControl(),
        categories: new FormControl(),
        summary: new FormControl(),
        description: new FormControl()
      }
    );
  }

  public onSubmit() {
    let movie = new Movie();
    movie.title = this.movieForm.get("title").value;
    movie.categories = this.movieForm.get("categories").value;
    movie.summary = this.movieForm.get("summary").value;
    movie.description= this.movieForm.get("description").value;

    this.movieService.createMovie(movie);
  }

}

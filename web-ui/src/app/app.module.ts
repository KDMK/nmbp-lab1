import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRouterModule} from "./module/router.module";
import {MovieListComponent} from './component/movie-list/movie-list.component';
import {HttpClientModule} from "@angular/common/http";
import { MovieCreateComponent } from './component/movie-create/movie-create.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { MovieSearchComponent } from './component/movie-search/movie-search.component';
import {RouterModule} from "@angular/router";
import { ReportByDayComponent } from './component/report-by-day/report-by-day.component';
import { ReportByHourComponent } from './component/report-by-hour/report-by-hour.component';

@NgModule({
  declarations: [
    AppComponent,
    MovieListComponent,
    MovieCreateComponent,
    MovieSearchComponent,
    ReportByDayComponent,
    ReportByHourComponent,
  ],
  imports: [
    BrowserModule,
    AppRouterModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

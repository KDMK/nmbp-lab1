import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MovieListComponent} from "../component/movie-list/movie-list.component";
import {MovieCreateComponent} from "../component/movie-create/movie-create.component";
import {MovieSearchComponent} from "../component/movie-search/movie-search.component";
import {ReportByDayComponent} from "../component/report-by-day/report-by-day.component";
import {ReportByHourComponent} from "../component/report-by-hour/report-by-hour.component";

const routes: Routes = [
  { path: 'movie/list', component: MovieListComponent },
  { path: 'movie/create', component: MovieCreateComponent },
  { path: 'movie/search', component: MovieSearchComponent },
  { path: 'report/range', component: ReportByDayComponent },
  { path: 'report/single', component: ReportByHourComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRouterModule {


}

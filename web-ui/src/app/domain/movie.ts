export class Movie {
  movieid: number;
  categories: string;
  description: string;
  summary: string;
  title: string;
  rank: number
}

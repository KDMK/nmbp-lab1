export class MovieReportStructureByDay {
  query: string;
  numOfQueriesByDay: Array<MovieReportDataStructure>
}

export interface MovieReportDataStructure {
  date: string;
  time: string;
  numOfQueries: number;
}

export class MovieReoportByHour {
  query: string;
  numOfQueriesByHour: Array<MovieReportDataStructure>;
}

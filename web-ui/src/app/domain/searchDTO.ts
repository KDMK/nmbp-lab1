import {Movie} from "./movie";

export class SearchDTO {
  searchResult: Array<Movie>;
  query: string
}

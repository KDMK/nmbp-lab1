import {Observable} from "rxjs";
import {Movie} from "../domain/movie";
import {QueryEntity} from "./query-parser.service";
import {SearchDTO} from "../domain/searchDTO";

export interface MovieService {

  getAllMovies(): Observable<Array<Movie>>
  getMovieById(id: number): Observable<Movie>
  fuzzySearch(term: string): Observable<Array<Movie>>
  movieSearch(queryEntity: QueryEntity): Observable<SearchDTO>
}

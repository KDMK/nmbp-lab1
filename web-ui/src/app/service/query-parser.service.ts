import {Injectable} from "@angular/core";

export interface QueryEntity {
  query: string
  queryElements: Array<string>,
  operator: string
}

@Injectable({providedIn: 'root'})
export class QueryParserService {
  private tokens = [];
  private curToken = "";

  private groupChars(curCharacter, tokenList) {
    if (curCharacter !== '"' && curCharacter !== " ") {
      this.curToken += curCharacter
    }

    if (curCharacter === " " || curCharacter === "\"") {
      tokenList.push(this.curToken);
      this.curToken = ""
    }

  }

  public parse(inputText: string) {
    inputText = inputText.trim();

    for (let i = 0; i < inputText.length; i++) {
      let c = inputText.charAt(i);

      if (c === '"') {
        c = inputText.charAt(++i);
        let compositeToken = [];

        while (c != '"') {
          this.groupChars(c, compositeToken);
          if (i < inputText.length) {
            c = inputText.charAt(++i)
          } else {
            throw "Invalid query: Could not find matching \""
          }
        }

        compositeToken.push(this.curToken);
        this.curToken = "";

        this.tokens.push(compositeToken)
      }

      if (c !== '"' && c !== " ") {
        this.curToken += c
      }

      if ((c === " " || i === (inputText.length - 1)) && this.curToken.length !== 0) {
        this.tokens.push(this.curToken);
        this.curToken = ""
      }
    }

    return this.tokens;
  }

  public makeExpression(tokens, operator) {
    let tokensCopy = Object.assign([], tokens);

    tokensCopy = tokensCopy.map((element) => {
      if (Array.isArray(element)) {
        let expression = element.join(" & ");
        return `(${expression})`
      } else {
        return element
      }
    });

    let op;
    switch (operator) {
      case "AND":
        op = " & ";
        break;
      case "OR":
        op = " | ";
        break;
      default:
        throw `Invalid operator ${operator}`
    }

    let retVal = {} as QueryEntity;
    retVal.query = tokensCopy.join(op);
    retVal.queryElements = tokensCopy;
    retVal.operator = operator;

    this.tokens = [];

    return retVal;
  }

}

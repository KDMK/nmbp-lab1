import {Injectable} from "@angular/core";
import {HttpClient, HttpRequest} from "@angular/common/http";
import {Movie} from "../domain/movie";
import {Observable} from "rxjs";
import {MovieService} from "./movie-service.interface";
import {QueryEntity} from "./query-parser.service";
import {SearchDTO} from "../domain/searchDTO";
import {Router} from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class MovieServiceImpl implements MovieService {

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  public getAllMovies(): Observable<Array<Movie>> {
    return this.httpClient.get("/api/movie") as Observable<Array<Movie>>
  }

  getMovieById(id: number): Observable<Movie> {
    return this.httpClient.get(`/api/movie/${id}`) as Observable<Movie>;
  }

  public movieSearch(queryEntity: QueryEntity): Observable<SearchDTO> {
    return this.httpClient.post(`/api/movie/search`, queryEntity) as Observable<SearchDTO>;
  }

  public fuzzySearch(term: string): Observable<Array<Movie>> {
    return this.httpClient.get(`/api/movie/fzf/${term}`) as Observable<Array<Movie>>;
  }

  public createMovie(movie: Movie) {
    this.httpClient.post("/api/movie", movie).subscribe(() => {
      console.log("Movie created");
      this.router.navigate(["/movie/search"]);
    });
  }
}

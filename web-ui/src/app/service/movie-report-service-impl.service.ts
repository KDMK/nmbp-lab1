import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {MovieReportStructureByDay} from "../domain/movieReportStructureByDay";
import {Observable} from "rxjs";

@Injectable({
  providedIn: "root"
})
export class MovieReportServiceImplService {

  constructor(private httpClient: HttpClient) {
  }

  public getReportForRange(startDate: String, endDate: String) {
    let requestBody = {
      startDate: startDate,
      endDate: endDate
    };

    return this.httpClient.post("/api/report/range", requestBody) as Observable<MovieReportStructureByDay[]>
  }

  public getReportForSingle(requestDate: String) {
    let requestBody = {
      requestDate: requestDate,
    };

    return this.httpClient.post("/api/report/single", requestBody) as Observable<MovieReportStructureByDay[]>
  }
}

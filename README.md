# Advanced databases, programming assignment 1

This repository contains solution of the first assignment for Advanced databases course at Faculty of electrotechnics and computing, University of Zagreb. 
You can find assignment text below.

---

## 1. Using PostgreSQL and test dataset

The task is to develop a simple document search engine, which will search the documents stored in a relational
database, and that will enable analysis of submitted queries (search conditions) using the knowledge on that
topic you have gained on the course. For the DBMS it is necessary to use PostgreSQL.
Follow the instructions published in the file PostgreSQL installation - instructions.pdf in repository 2018/2019
Instructions and install PostgreSQL DBMS on your personal computer
Upon installation, you can run SQL queries against PostgreSQL DBMS using some of SQL editors customized for
working with PostgreSQL, e.g. PgAdmin (probably you have installed PgAdmin together with the DMBS
installation)..
Using PgAdmin connect to PostgreSQL server installed and create a new database (right-click on Databases -
Create - Database; Name: asYouWish, Owner: postgres). Select the database you want to work with and open
a query window (Tools - Query Tool).
If you prefere to use the dd.mm.yyyy format for the Date data type you can do that performing the following
command:
set DateStyle ='German, DMY';
For the application development and testing you will need textual data, so you can use data about movies that
we have prepared for you for this project.
Using the file movie.sql from the repository 2018/2019 -> Project -> 1st project - Text search and advanced
SQL you can create table movie (containing 1000 records about the movies). You can execute SQL statements
using PgAdmin.
The table movie contains 4 textual attributes: movie name, movie category, summary and description. This
table scheme is not sufficient for satisfying all of the project demands. You are free to extend it.

## 2. Description of application functionalities

The task is to develop an application (web/desktop/mobile) which will enable three things:
    1. Insertion of the text content into PostgreSQL database containing an arbitrary data scheme (minimal
       but rich enough for the demonstration of all project exercises)
    2. Search the text content stored in the database
    3. Analysis of submitted queries in a given period of time
       Application design is arbitrary as well as the choice of
       technologies you will use to build it.
       
### 2.1 Insertion of the text-type content in the database
Store content in a PostgreSQL database. Use texts in the
English language due to full support for Full Text Search and Fuzzy Text Search. 

### 2.2 Searching the text content stored in the database
The task is to enable retreival of the documents that contain the searched sample (samples) in normalized
form (Based on morphology & semantic), but also documents that contain any form of the word from the
searched text (sample) should be retreived too. This kind of search involves word normalization, removing the
stop-words, etc.
In the application it is necessary to:
  1. support linking of the given search text by Boolean operator AND
  2. support linking of the given search text by Boolean operator OR
  3. display the SQL query (generated based on the user's search conditions (search samples + logical
     operator)) by which all of the relevant documents were retrieved
  4. display informative data for the document to the user. Searched words found in document that
     qualified it as the result should be bolded. Also, display the document rank. Note that the words
     Legend Tarzan and Lord are bolded in the document shown in the picture. Study how PostgreSQL
     implements document ranking functions (we recommend contents on
     http://shisaa.jp/postset/postgresql-full-text-search-part-3.html). In your application, use a ranking
     function you find the most suitable, using most suitable parameters. Try to apply more sophisticated
     approach of implementing ranking functions than the one shown in the picture in which function
     ts_rank was called with the "default" parameters.
  5. enable phrase search (multiple strings within quotation marks) and "simple" words combined with
     Boolean operators AND and OR.
     The picture below shows one of the ways to find documents containing phrases "Legend of Tarzan" or
     "Lord of" or word Dance.
  6. implement autocomplete that will, during the input, offer to the user e.g. up to 5
     reasonable completions of their search conditions. Come up with your own
     algorithm that will support this feature. For the implementation of this
     functionality you can assume that the user needs to be offered completions based
     on words/phrases used in the attribute movie.summary. Although it is possible to
     implement this functionality in many ways, for the simplicity you may choose one
     of the offered two:
       - implementation of search tree (see: https://www.postgresql.org/docs/9.6/static/ltree.html)
       - using some of the fuzzy text search possibilities in PostgreSQL (see e.g.:
         https://www.sitepoint.com/awesome-autocomplete-trigram-search-in-rails-and-postgresql/)

Model the database so the search is speeded up as much as possible - e.g. storing the additional normalized
text, creating specialized indexes (as many as needed but which are useful for speeding up the search).
For the implementation of the search and autocomplete, chose operators and functions offered by PostgreSQL
which you find the most suitable. You must be able to explain and argue your choices.


## 3. Analysis of queries submitted in a given period of time
To make the analysis possible it is necessary to log the queries (including submission timestamp) provided by
users. It usually has to be taken into consideration during both database and application design. Assess what
parameters about the submitted queries would be wise to record.
It is necessary to allow to the user:
  1. specifying a time period in which the analysis should be conducted, in the form:
     date from – date to (e.g. 10/10/2014 -13/10/2014)
  2. specifying the granularity of analysis:
    * day or
    * hour

Using the advanced features of SQL (pivoting) produce a report containing the number of submitting of a
specific query for the specified period (e.g. 10/10/2014 -13/10/2014) per days or per hours, based on what the
user has chosen. 

*Original text of this assignment is posted on http://www.fer.unizg.hr/en/course/advdat* 
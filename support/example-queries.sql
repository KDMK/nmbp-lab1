--working cross-tab example
SELECT * FROM crosstab($$
  SELECT query, DATE_TRUNC('day', created_at) date,
					   count(query) num FROM nmbp.queries
    WHERE created_at > to_date('10.01.2018', 'DD.MM.YYYY')
    AND created_at < to_date($$ || '20.01.2018 || $$, 'DD.MM.YYYY')
  GROUP BY query, date
  ORDER BY 1;
  $$,$$
    SELECT GENERATE_SERIES (
      to_timestamp('10.01.2018', 'DD.MM.YYYY'),  -- initial day
      to_timestamp('20.01.2018', 'DD.MM.YYYY'),  -- final day
      '1 day'
    )::Timestamp without time zone
  $$
) AS bla(
  entity TEXT
, "day 1" NUMERIC
, "day 2" NUMERIC
, "day 3" NUMERIC
, "day 4" NUMERIC
, "day 5" NUMERIC
, "day 6" NUMERIC
, "day 7" NUMERIC
, "day 8" NUMERIC
, "day 9" NUMERIC
, "day 10" NUMERIC
, "day 11" NUMERIC
);

--Crosstab for hours
SELECT * FROM CROSSTAB($$
  SELECT query, extract(hour from created_at) as hour, count(query) num FROM nmbp.queries
    WHERE created_at > to_date('10.01.2018', 'DD.MM.YYYY')
    AND created_at < to_date('11.01.2018', 'DD.MM.YYYY')
  GROUP BY query, hour
  ORDER BY 2,1;
  $$,$$
    SELECT GENERATE_SERIES (0,23,1)
  $$
) AS bla(
  entity TEXT
, "day 00:00" BIGINT
, "day 01:00" NUMERIC
, "day 02:00" NUMERIC
, "day 03:00" NUMERIC
, "day 04:00" NUMERIC
, "day 05:00" NUMERIC
, "day 06:00" NUMERIC
, "day 07:00" NUMERIC
, "day 08:00" NUMERIC
, "day 09:00" NUMERIC
, "day 10:00" NUMERIC
, "day 11:00" NUMERIC
, "day 12:00" NUMERIC
, "day 13:00" NUMERIC
, "day 14:00" NUMERIC
, "day 15:00" NUMERIC
, "day 16:00" NUMERIC
, "day 17:00" NUMERIC
, "day 18:00" NUMERIC
, "day 19:00" NUMERIC
, "day 20:00" NUMERIC
, "day 21:00" NUMERIC
, "day 22:00" NUMERIC
, "day 23:00" NUMERIC
);
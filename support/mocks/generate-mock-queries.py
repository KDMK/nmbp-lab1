import random
import jinja2
import time

MIN_DATE = 1508430533
MAX_DATE = 1539966533

class Query:
  def __init__(self, query, timestamp, source):
    self.queryString = query
    self.timestamp = timestamp
    self.source = source

def generateRandomIPs(numOfAddresses):
  ipAddresses = []
  for i in range(0, numOfAddresses):
    ipAddress = str(random.randint(100,220)) + "." + str(random.randint(0,254)) + "." + str(random.randint(0,254)) + "." + str(random.randint(1,254))
    ipAddresses.append(ipAddress)
  return ipAddresses

def main():
  print("Random queries generator v1")
  print("=============================================================")
  print("[INFO] Loading words from /usr/share/dict/american-english...")
  with open('/usr/share/dict/american-english') as f:
    words = [word for line in f for word in line.split()]
  print("[INFO] Done")

  random.seed(int(time.time()))

  print("[INFO] Generating random IPs...")
  ips = generateRandomIPs(1500)
  print("[INFO] Done")

  print("[INFO] Generating random queries")
  queryStrings = []
  for i in range(0,1000):
    selectedWords = []
    for j in range(0, random.randint(1,5)):
      selectedWords.append(words[random.randint(0, len(words) - 1)].replace("'", "''"))
    operator = " & " if (random.randint(0,100) < 40) else " | "
    queryStrings.append(operator.join(selectedWords))
  print("[INFO] Done")

  queries = []
  for i in range(0, 10000):
    query = Query(queryStrings[random.randint(0, len(queryStrings) - 1)], random.randint(MIN_DATE, MAX_DATE), ips[random.randint(0, len(ips) - 1)])
    queries.append(query)

  print("[INFO] Generating SQL insert script from template...")
  templateLoader = jinja2.FileSystemLoader(searchpath="./")
  templateEnv = jinja2.Environment(loader=templateLoader)
  TEMPLATE_FILE = "queries.j2.sql"
  template = templateEnv.get_template(TEMPLATE_FILE)
  outputText = template.render(queries=queries)
  print("[INFO] Done")

  print("[INFO] Writing template to queries.sql")
  f = open('./queries.sql', 'w')
  f.writelines(outputText)
  f.close()
  print("[INFO] Done")

main()



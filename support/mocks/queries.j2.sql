{% for query in queries %}
INSERT INTO nmbp.queries(query, created_at, source) VALUES ('{{ query.queryString }}', to_timestamp({{ query.timestamp }}), '{{ query.source }}');
{%- endfor %}
